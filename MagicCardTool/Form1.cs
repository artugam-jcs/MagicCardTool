﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Drawing.Imaging;

namespace MagicCardTool
{
    public partial class mMainForm : Form
    {
        private static int MIN_DAMAGE = 1;
        private static int MAX_DAMAGE = 10;

        private static int MIN_HEALTH = 1;
        private static int MAX_HEALTH = 10;

        private static string DEFAULT_SAVE_FILE_EXTENSION = "xml";

        private string mSavePathIMG = "";

        /// <summary>
        /// Magic and Gathering's land type list here...
        /// </summary>
        public enum LandType
        {
            SWAMP,
            FOREST,
            ISLAND,
            PLAINS,
            MOUNTAIN
        };

        /// <summary>
        /// Magic and gathering card data design here...
        /// </summary>
        [Serializable]
        public struct CardData
        {
            public string imageFilePath;
            public LandType frame;

            public string cardName;
            public string effectText;
            public string monsterType;

            public int damage;
            public int health;
        };

        private string mImageFilePath = "";


        public mMainForm()
        {
            InitializeComponent();

            InitForm();
        }

        /// <summary>
        /// Initialize the form.
        /// </summary>
        private void InitForm()
        {
            // Add all land type to frame type combo box.
            foreach (var e in GetValues<LandType>())
                mFrameTypeCB.Items.Add(e);
            // give a default value.
            mFrameTypeCB.SelectedItem = LandType.MOUNTAIN;

            // make label transparent
            {
                var pos = this.PointToScreen(mCardNameLabel.Location);
                pos = mFramePB.PointToClient(pos);
                mCardNameLabel.Parent = mFramePB;
                mCardNameLabel.Location = pos;
                mCardNameLabel.BackColor = Color.Transparent;

                pos = this.PointToScreen(mDamageLabel.Location);
                pos = mFramePB.PointToClient(pos);
                mDamageLabel.Parent = mFramePB;
                mDamageLabel.Location = pos;
                mDamageLabel.BackColor = Color.Transparent;

                pos = this.PointToScreen(mHealthLabel.Location);
                pos = mFramePB.PointToClient(pos);
                mHealthLabel.Parent = mFramePB;
                mHealthLabel.Location = pos;
                mHealthLabel.BackColor = Color.Transparent;

                pos = this.PointToScreen(mBackSlashLabel.Location);
                pos = mFramePB.PointToClient(pos);
                mBackSlashLabel.Parent = mFramePB;
                mBackSlashLabel.Location = pos;
                mBackSlashLabel.BackColor = Color.Transparent;

                pos = this.PointToScreen(mMobTypeLabel.Location);
                pos = mFramePB.PointToClient(pos);
                mMobTypeLabel.Parent = mFramePB;
                mMobTypeLabel.Location = pos;
                mMobTypeLabel.BackColor = Color.Transparent;

                pos = this.PointToScreen(mCardImagePB.Location);
                pos = mFramePB.PointToClient(pos);
                mCardImagePB.Parent = mFramePB;
                mCardImagePB.Location = pos;
                //mCardImagePB.BackColor = Color.Transparent;

                pos = this.PointToScreen(mCardEffectLabel.Location);
                pos = mFramePB.PointToClient(pos);
                mCardEffectLabel.Parent = mFramePB;
                mCardEffectLabel.Location = pos;
                mCardEffectLabel.BackColor = Color.Transparent;
            }

            // initialize damage/health combo box
            {
                for (int count = MIN_DAMAGE;
                    count <= MAX_DAMAGE;
                    ++count)
                {
                    mDamageCB.Items.Add(count);
                }
                mDamageCB.SelectedIndex = 2;

                for (int count = MIN_HEALTH;
                    count <= MAX_HEALTH;
                    ++count)
                {
                    mHealthCB.Items.Add(count);
                }
                mHealthCB.SelectedIndex = 0;
            }
        }

        private void mFrameTypeCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            SwitchFrame((LandType)mFrameTypeCB.SelectedItem);
        }

        /// <summary>
        /// Switch the frame for the picture box.
        /// </summary>
        /// <param name="inType"> Type of the frame. </param>
        public void SwitchFrame(LandType inType)
        {
            switch (inType)
            {
                case LandType.FOREST:
                    mFramePB.Image = Properties.Resources.forest_monster_frame;
                    break;
                case LandType.ISLAND:
                    mFramePB.Image = Properties.Resources.island_monster_frame;
                    break;
                case LandType.MOUNTAIN:
                    mFramePB.Image = Properties.Resources.mountain_monster_frame;
                    break;
                case LandType.PLAINS:
                    mFramePB.Image = Properties.Resources.plains_monster_frame;
                    break;
                case LandType.SWAMP:
                    mFramePB.Image = Properties.Resources.swamp_monster_frame;
                    break;
            }
        }

        /// <summary>
        /// Enum typed version casting.
        /// Source: http://stackoverflow.com/questions/972307/can-you-loop-through-all-enum-values
        /// </summary>
        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        private void mCardNameTB_TextChanged(object sender, EventArgs e)
        {
            mCardNameLabel.Text = mCardNameTB.Text;
        }

        private void mDamageCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            mDamageLabel.Text = mDamageCB.SelectedItem.ToString();
        }

        private void mHealthCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            mHealthLabel.Text = mHealthCB.SelectedItem.ToString();
        }

        private void mLoadImageBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();

            /* Set info of the dialog. */
            openDialog.DefaultExt = "png";
            openDialog.Filter = "All files (*.*)|*.*|JPEG (*.jpg)|*jpg|PNG files (*.png)|*.png";

            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                mCardImagePB.Load(openDialog.FileName);

                mImageFilePath = openDialog.FileName;
            }
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void exportCardXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();

            saveDialog.DefaultExt = DEFAULT_SAVE_FILE_EXTENSION;

            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(CardData));

                // Truncate -> Delete all previous content
                FileStream stream = null;
                if (File.Exists(saveDialog.FileName))
                    stream = new FileStream(saveDialog.FileName, FileMode.Truncate);
                else
                    stream = new FileStream(saveDialog.FileName, FileMode.OpenOrCreate);

                CardData currentCardData = GetCurrentCardData();

                serializer.Serialize(stream, currentCardData);

                stream.Close();
            }
        }

        private void loadCardXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();

            /* Set info of the dialog. */
            openDialog.DefaultExt = "xml";
            openDialog.InitialDirectory = "D:\\Users\\JSHEN3\\Downloads";
            openDialog.Filter = "XML (*.xml)|*xml|SAVE files (*.save)|*.save";

            //openDialog.Multiselect = true;

            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                LoadCardXML(openDialog.FileName);
            }
        }

        /// <summary>
        /// Load the xml and update the card info.
        /// </summary>
        /// <param name="inFilePath"> file path to xml. </param>
        public void LoadCardXML(string inFilePath)
        {
            // load the init save file.
            if (File.Exists(inFilePath))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(CardData));

                FileStream stream = new FileStream(inFilePath, FileMode.Open);

                CardData tmpImageData = (CardData)serializer.Deserialize(stream);

                // TODO: update card view.
                SetCurrentCardData(tmpImageData);

                stream.Close();
            }
        }

        /// <summary>
        /// Save the card data as XML.
        /// </summary>
        /// <param name="inFilePath"> path the xml going to be. </param>
        /// <param name="inCardData"> Card data struct going to be export. </param>
        public void SaveCardAsXML(string inFilePath, CardData inCardData)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CardData));

            // Truncate -> Delete all previous content
            FileStream stream = null;
            if (!File.Exists(inFilePath))
                stream = new FileStream(inFilePath, FileMode.OpenOrCreate);
            else
                stream = new FileStream(inFilePath, FileMode.Truncate);

            serializer.Serialize(stream, inCardData);

            stream.Close();
        }

        /// <summary>
        /// Override current card data.
        /// </summary>
        /// <param name="inCardData"> card data to be set. </param>
        public void SetCurrentCardData(CardData inCardData)
        {
            if (inCardData.imageFilePath == "" || !File.Exists(inCardData.imageFilePath))
                return;

            // load image 
            {
                mImageFilePath = inCardData.imageFilePath;
                mCardImagePB.Load(mImageFilePath);
            }

            mFrameTypeCB.SelectedItem = inCardData.frame;

            mCardNameTB.Text = inCardData.cardName;
            mCardEffectRTB.Text = inCardData.effectText;
            mMobTypeTB.Text = inCardData.monsterType;

            mDamageCB.SelectedItem = inCardData.damage;
            mHealthCB.SelectedItem = inCardData.health;
        }

        /// <summary>
        /// Return the card data struct for current card that
        /// are design/load.
        /// </summary>
        /// <returns></returns>
        public CardData GetCurrentCardData()
        {
            CardData cardData = new CardData();

            cardData.imageFilePath = mImageFilePath;
            cardData.frame = (LandType)mFrameTypeCB.SelectedItem;

            cardData.cardName = mCardNameLabel.Text;
            cardData.effectText = mCardEffectRTB.Text;
            cardData.monsterType = mMobTypeLabel.Text;

            cardData.damage = (int)mDamageCB.SelectedItem;
            cardData.health = (int)mHealthCB.SelectedItem;

            return cardData;
        }

        private void mMobTypeTB_TextChanged(object sender, EventArgs e)
        {
            mMobTypeLabel.Text = mMobTypeTB.Text;
        }

        private void mResetBtn_Click(object sender, EventArgs e)
        {
            ResetCurrentCardData();
        }

        /// <summary>
        /// Reset all card info setting.
        /// </summary>
        public void ResetCurrentCardData()
        {
            // load image 
            {
                mCardImagePB.Image = null;
            }

            mFrameTypeCB.SelectedItem = 0;

            mCardNameTB.Text = "Card Name";
            mCardEffectRTB.Text = "Effect Text";
            mMobTypeTB.Text = "Monster Type";

            mDamageCB.SelectedItem = 1;
            mHealthCB.SelectedItem = 1;
        }

        private void exportCardIMGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();

            saveDialog.DefaultExt = DEFAULT_SAVE_FILE_EXTENSION;

            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                this.mSavePathIMG = saveDialog.FileName;

                int x = 12 + 8;
                int y = 58 + 29;
                int width = 341 - 1;
                int height = 489 - 14;

                //The image we will be drawing on then passing to picturebox
                Bitmap bmp = new Bitmap(width, height);

                using (Graphics g = Graphics.FromImage(bmp))
                {
                    using (Bitmap b = new Bitmap(this.Width, this.Height))
                    {
                        //captures the Form screenschot, and saves it into Bitmap b
                        this.DrawToBitmap(b, new Rectangle(0, 0, this.Width, this.Height));

                        //this draws the image from Bitmap b starting at the specified location to Bitmap bmp 
                        g.DrawImageUnscaled(b, -x, -y);

                        bmp.Save(this.mSavePathIMG, ImageFormat.Png);
                    }
                }
            }
        }

        private void mCardEffectRTB_TextChanged(object sender, EventArgs e)
        {
            mCardEffectLabel.Text = mCardEffectRTB.Text;
        }
    }

}
