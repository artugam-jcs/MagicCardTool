﻿namespace MagicCardTool
{
    partial class mMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mFramePB = new System.Windows.Forms.PictureBox();
            this.mAutoGenerateBtn = new System.Windows.Forms.Button();
            this.mFrameTypeCB = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.mCardInfoGB = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.mMobTypeTB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.mHealthTextLabel = new System.Windows.Forms.Label();
            this.mHealthCB = new System.Windows.Forms.ComboBox();
            this.mDamageCB = new System.Windows.Forms.ComboBox();
            this.mDamageTextLabel = new System.Windows.Forms.Label();
            this.mImageLabel = new System.Windows.Forms.Label();
            this.mLoadImageBtn = new System.Windows.Forms.Button();
            this.mCardEffectRTB = new System.Windows.Forms.RichTextBox();
            this.mCardNameTBLabel = new System.Windows.Forms.Label();
            this.mCardNameTB = new System.Windows.Forms.TextBox();
            this.mCardNameLabel = new System.Windows.Forms.Label();
            this.mCardImagePB = new System.Windows.Forms.PictureBox();
            this.mDamageLabel = new System.Windows.Forms.Label();
            this.mMobTypeLabel = new System.Windows.Forms.Label();
            this.mHealthLabel = new System.Windows.Forms.Label();
            this.mBackSlashLabel = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.mMainSS = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadCardXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportCardXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportCardIMGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mResetBtn = new System.Windows.Forms.Button();
            this.mCardEffectLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.mFramePB)).BeginInit();
            this.mCardInfoGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mCardImagePB)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mFramePB
            // 
            this.mFramePB.Image = global::MagicCardTool.Properties.Resources.mountain_monster_frame;
            this.mFramePB.Location = new System.Drawing.Point(12, 34);
            this.mFramePB.Name = "mFramePB";
            this.mFramePB.Size = new System.Drawing.Size(341, 519);
            this.mFramePB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.mFramePB.TabIndex = 0;
            this.mFramePB.TabStop = false;
            // 
            // mAutoGenerateBtn
            // 
            this.mAutoGenerateBtn.Location = new System.Drawing.Point(509, 528);
            this.mAutoGenerateBtn.Name = "mAutoGenerateBtn";
            this.mAutoGenerateBtn.Size = new System.Drawing.Size(113, 25);
            this.mAutoGenerateBtn.TabIndex = 1;
            this.mAutoGenerateBtn.Text = "Auto Generate";
            this.mAutoGenerateBtn.UseVisualStyleBackColor = true;
            // 
            // mFrameTypeCB
            // 
            this.mFrameTypeCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mFrameTypeCB.FormattingEnabled = true;
            this.mFrameTypeCB.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.mFrameTypeCB.Location = new System.Drawing.Point(121, 18);
            this.mFrameTypeCB.Name = "mFrameTypeCB";
            this.mFrameTypeCB.Size = new System.Drawing.Size(121, 21);
            this.mFrameTypeCB.TabIndex = 2;
            this.mFrameTypeCB.SelectedIndexChanged += new System.EventHandler(this.mFrameTypeCB_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("PMingLiU", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(16, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 14);
            this.label1.TabIndex = 3;
            this.label1.Text = "Frame Type: ";
            // 
            // mCardInfoGB
            // 
            this.mCardInfoGB.Controls.Add(this.label3);
            this.mCardInfoGB.Controls.Add(this.mMobTypeTB);
            this.mCardInfoGB.Controls.Add(this.label2);
            this.mCardInfoGB.Controls.Add(this.mHealthTextLabel);
            this.mCardInfoGB.Controls.Add(this.mHealthCB);
            this.mCardInfoGB.Controls.Add(this.mDamageCB);
            this.mCardInfoGB.Controls.Add(this.mDamageTextLabel);
            this.mCardInfoGB.Controls.Add(this.mImageLabel);
            this.mCardInfoGB.Controls.Add(this.mLoadImageBtn);
            this.mCardInfoGB.Controls.Add(this.mCardEffectRTB);
            this.mCardInfoGB.Controls.Add(this.mCardNameTBLabel);
            this.mCardInfoGB.Controls.Add(this.mCardNameTB);
            this.mCardInfoGB.Controls.Add(this.mFrameTypeCB);
            this.mCardInfoGB.Controls.Add(this.label1);
            this.mCardInfoGB.Location = new System.Drawing.Point(377, 35);
            this.mCardInfoGB.Name = "mCardInfoGB";
            this.mCardInfoGB.Size = new System.Drawing.Size(248, 488);
            this.mCardInfoGB.TabIndex = 4;
            this.mCardInfoGB.TabStop = false;
            this.mCardInfoGB.Text = "Card Information";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.label3.Location = new System.Drawing.Point(16, 147);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 14);
            this.label3.TabIndex = 14;
            this.label3.Text = "Card Effect: ";
            // 
            // mMobTypeTB
            // 
            this.mMobTypeTB.Location = new System.Drawing.Point(114, 117);
            this.mMobTypeTB.Name = "mMobTypeTB";
            this.mMobTypeTB.Size = new System.Drawing.Size(125, 20);
            this.mMobTypeTB.TabIndex = 13;
            this.mMobTypeTB.Text = "Creature - Human Sodlier";
            this.mMobTypeTB.TextChanged += new System.EventHandler(this.mMobTypeTB_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("PMingLiU", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(16, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Monster Type: ";
            // 
            // mHealthTextLabel
            // 
            this.mHealthTextLabel.AutoSize = true;
            this.mHealthTextLabel.Font = new System.Drawing.Font("PMingLiU", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.mHealthTextLabel.Location = new System.Drawing.Point(139, 87);
            this.mHealthTextLabel.Name = "mHealthTextLabel";
            this.mHealthTextLabel.Size = new System.Drawing.Size(51, 14);
            this.mHealthTextLabel.TabIndex = 11;
            this.mHealthTextLabel.Text = "Health: ";
            // 
            // mHealthCB
            // 
            this.mHealthCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mHealthCB.FormattingEnabled = true;
            this.mHealthCB.Location = new System.Drawing.Point(196, 83);
            this.mHealthCB.Name = "mHealthCB";
            this.mHealthCB.Size = new System.Drawing.Size(46, 21);
            this.mHealthCB.TabIndex = 10;
            this.mHealthCB.SelectedIndexChanged += new System.EventHandler(this.mHealthCB_SelectedIndexChanged);
            // 
            // mDamageCB
            // 
            this.mDamageCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mDamageCB.FormattingEnabled = true;
            this.mDamageCB.Location = new System.Drawing.Point(79, 83);
            this.mDamageCB.Name = "mDamageCB";
            this.mDamageCB.Size = new System.Drawing.Size(46, 21);
            this.mDamageCB.TabIndex = 9;
            this.mDamageCB.SelectedIndexChanged += new System.EventHandler(this.mDamageCB_SelectedIndexChanged);
            // 
            // mDamageTextLabel
            // 
            this.mDamageTextLabel.AutoSize = true;
            this.mDamageTextLabel.Font = new System.Drawing.Font("PMingLiU", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.mDamageTextLabel.Location = new System.Drawing.Point(16, 87);
            this.mDamageTextLabel.Name = "mDamageTextLabel";
            this.mDamageTextLabel.Size = new System.Drawing.Size(59, 14);
            this.mDamageTextLabel.TabIndex = 8;
            this.mDamageTextLabel.Text = "Damage: ";
            // 
            // mImageLabel
            // 
            this.mImageLabel.AutoSize = true;
            this.mImageLabel.Font = new System.Drawing.Font("PMingLiU", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.mImageLabel.Location = new System.Drawing.Point(6, 432);
            this.mImageLabel.Name = "mImageLabel";
            this.mImageLabel.Size = new System.Drawing.Size(48, 14);
            this.mImageLabel.TabIndex = 7;
            this.mImageLabel.Text = "Image: ";
            // 
            // mLoadImageBtn
            // 
            this.mLoadImageBtn.Location = new System.Drawing.Point(44, 456);
            this.mLoadImageBtn.Name = "mLoadImageBtn";
            this.mLoadImageBtn.Size = new System.Drawing.Size(172, 25);
            this.mLoadImageBtn.TabIndex = 6;
            this.mLoadImageBtn.Text = "Load Image";
            this.mLoadImageBtn.UseVisualStyleBackColor = true;
            this.mLoadImageBtn.Click += new System.EventHandler(this.mLoadImageBtn_Click);
            // 
            // mCardEffectRTB
            // 
            this.mCardEffectRTB.AcceptsTab = true;
            this.mCardEffectRTB.Font = new System.Drawing.Font("PMingLiU", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.mCardEffectRTB.Location = new System.Drawing.Point(9, 178);
            this.mCardEffectRTB.Name = "mCardEffectRTB";
            this.mCardEffectRTB.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.mCardEffectRTB.Size = new System.Drawing.Size(230, 130);
            this.mCardEffectRTB.TabIndex = 7;
            this.mCardEffectRTB.Text = "If you have this card on your hand you won the game instantly.";
            this.mCardEffectRTB.TextChanged += new System.EventHandler(this.mCardEffectRTB_TextChanged);
            // 
            // mCardNameTBLabel
            // 
            this.mCardNameTBLabel.AutoSize = true;
            this.mCardNameTBLabel.Font = new System.Drawing.Font("PMingLiU", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.mCardNameTBLabel.Location = new System.Drawing.Point(16, 50);
            this.mCardNameTBLabel.Name = "mCardNameTBLabel";
            this.mCardNameTBLabel.Size = new System.Drawing.Size(76, 14);
            this.mCardNameTBLabel.TabIndex = 5;
            this.mCardNameTBLabel.Text = "Card Name: ";
            // 
            // mCardNameTB
            // 
            this.mCardNameTB.Location = new System.Drawing.Point(121, 50);
            this.mCardNameTB.Name = "mCardNameTB";
            this.mCardNameTB.Size = new System.Drawing.Size(120, 20);
            this.mCardNameTB.TabIndex = 4;
            this.mCardNameTB.Text = "Card Name";
            this.mCardNameTB.TextChanged += new System.EventHandler(this.mCardNameTB_TextChanged);
            // 
            // mCardNameLabel
            // 
            this.mCardNameLabel.AutoSize = true;
            this.mCardNameLabel.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.mCardNameLabel.Font = new System.Drawing.Font("PMingLiU", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.mCardNameLabel.Location = new System.Drawing.Point(48, 87);
            this.mCardNameLabel.Name = "mCardNameLabel";
            this.mCardNameLabel.Size = new System.Drawing.Size(77, 14);
            this.mCardNameLabel.TabIndex = 5;
            this.mCardNameLabel.Text = "Card Name";
            // 
            // mCardImagePB
            // 
            this.mCardImagePB.BackColor = System.Drawing.SystemColors.Desktop;
            this.mCardImagePB.Location = new System.Drawing.Point(40, 110);
            this.mCardImagePB.Name = "mCardImagePB";
            this.mCardImagePB.Size = new System.Drawing.Size(285, 211);
            this.mCardImagePB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.mCardImagePB.TabIndex = 8;
            this.mCardImagePB.TabStop = false;
            // 
            // mDamageLabel
            // 
            this.mDamageLabel.AutoSize = true;
            this.mDamageLabel.Font = new System.Drawing.Font("PMingLiU", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.mDamageLabel.Location = new System.Drawing.Point(277, 486);
            this.mDamageLabel.Name = "mDamageLabel";
            this.mDamageLabel.Size = new System.Drawing.Size(14, 15);
            this.mDamageLabel.TabIndex = 10;
            this.mDamageLabel.Text = "2";
            // 
            // mMobTypeLabel
            // 
            this.mMobTypeLabel.AutoSize = true;
            this.mMobTypeLabel.Font = new System.Drawing.Font("PMingLiU", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.mMobTypeLabel.Location = new System.Drawing.Point(48, 329);
            this.mMobTypeLabel.Name = "mMobTypeLabel";
            this.mMobTypeLabel.Size = new System.Drawing.Size(171, 14);
            this.mMobTypeLabel.TabIndex = 11;
            this.mMobTypeLabel.Text = "Creature - Human Sodlier";
            // 
            // mHealthLabel
            // 
            this.mHealthLabel.AutoSize = true;
            this.mHealthLabel.Font = new System.Drawing.Font("PMingLiU", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.mHealthLabel.Location = new System.Drawing.Point(302, 486);
            this.mHealthLabel.Name = "mHealthLabel";
            this.mHealthLabel.Size = new System.Drawing.Size(14, 15);
            this.mHealthLabel.TabIndex = 12;
            this.mHealthLabel.Text = "3";
            // 
            // mBackSlashLabel
            // 
            this.mBackSlashLabel.AutoSize = true;
            this.mBackSlashLabel.Font = new System.Drawing.Font("PMingLiU", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.mBackSlashLabel.Location = new System.Drawing.Point(291, 486);
            this.mBackSlashLabel.Name = "mBackSlashLabel";
            this.mBackSlashLabel.Size = new System.Drawing.Size(11, 15);
            this.mBackSlashLabel.TabIndex = 13;
            this.mBackSlashLabel.Text = "/";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mMainSS});
            this.statusStrip1.Location = new System.Drawing.Point(0, 561);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(637, 22);
            this.statusStrip1.TabIndex = 14;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // mMainSS
            // 
            this.mMainSS.Name = "mMainSS";
            this.mMainSS.Size = new System.Drawing.Size(36, 17);
            this.mMainSS.Text = "None";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(637, 24);
            this.menuStrip1.TabIndex = 15;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadCardXMLToolStripMenuItem,
            this.exportCardXMLToolStripMenuItem,
            this.exportCardIMGToolStripMenuItem,
            this.quitToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // loadCardXMLToolStripMenuItem
            // 
            this.loadCardXMLToolStripMenuItem.Name = "loadCardXMLToolStripMenuItem";
            this.loadCardXMLToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.loadCardXMLToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.loadCardXMLToolStripMenuItem.Text = "Load Card (XML)";
            this.loadCardXMLToolStripMenuItem.Click += new System.EventHandler(this.loadCardXMLToolStripMenuItem_Click);
            // 
            // exportCardXMLToolStripMenuItem
            // 
            this.exportCardXMLToolStripMenuItem.Name = "exportCardXMLToolStripMenuItem";
            this.exportCardXMLToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.exportCardXMLToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.exportCardXMLToolStripMenuItem.Text = "Export Card (XML)";
            this.exportCardXMLToolStripMenuItem.Click += new System.EventHandler(this.exportCardXMLToolStripMenuItem_Click);
            // 
            // exportCardIMGToolStripMenuItem
            // 
            this.exportCardIMGToolStripMenuItem.Name = "exportCardIMGToolStripMenuItem";
            this.exportCardIMGToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.exportCardIMGToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.exportCardIMGToolStripMenuItem.Text = "Export Card (IMG)";
            this.exportCardIMGToolStripMenuItem.Click += new System.EventHandler(this.exportCardIMGToolStripMenuItem_Click);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.quitToolStripMenuItem.Text = "&Quit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
            // 
            // mResetBtn
            // 
            this.mResetBtn.Location = new System.Drawing.Point(377, 528);
            this.mResetBtn.Name = "mResetBtn";
            this.mResetBtn.Size = new System.Drawing.Size(125, 25);
            this.mResetBtn.TabIndex = 14;
            this.mResetBtn.Text = "Reset";
            this.mResetBtn.UseVisualStyleBackColor = true;
            this.mResetBtn.Click += new System.EventHandler(this.mResetBtn_Click);
            // 
            // mCardEffectLabel
            // 
            this.mCardEffectLabel.Location = new System.Drawing.Point(47, 359);
            this.mCardEffectLabel.Name = "mCardEffectLabel";
            this.mCardEffectLabel.Size = new System.Drawing.Size(269, 120);
            this.mCardEffectLabel.TabIndex = 16;
            this.mCardEffectLabel.Text = "If you have this card on your hand you won the game instantly.";
            // 
            // mMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 583);
            this.Controls.Add(this.mCardEffectLabel);
            this.Controls.Add(this.mResetBtn);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.mBackSlashLabel);
            this.Controls.Add(this.mHealthLabel);
            this.Controls.Add(this.mMobTypeLabel);
            this.Controls.Add(this.mDamageLabel);
            this.Controls.Add(this.mCardImagePB);
            this.Controls.Add(this.mCardNameLabel);
            this.Controls.Add(this.mCardInfoGB);
            this.Controls.Add(this.mAutoGenerateBtn);
            this.Controls.Add(this.mFramePB);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "mMainForm";
            this.Text = "Magic Card Tool";
            ((System.ComponentModel.ISupportInitialize)(this.mFramePB)).EndInit();
            this.mCardInfoGB.ResumeLayout(false);
            this.mCardInfoGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mCardImagePB)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox mFramePB;
        private System.Windows.Forms.Button mAutoGenerateBtn;
        private System.Windows.Forms.ComboBox mFrameTypeCB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox mCardInfoGB;
        private System.Windows.Forms.Label mCardNameLabel;
        private System.Windows.Forms.RichTextBox mCardEffectRTB;
        private System.Windows.Forms.Label mCardNameTBLabel;
        private System.Windows.Forms.TextBox mCardNameTB;
        private System.Windows.Forms.PictureBox mCardImagePB;
        private System.Windows.Forms.Label mDamageLabel;
        private System.Windows.Forms.Label mMobTypeLabel;
        private System.Windows.Forms.Button mLoadImageBtn;
        private System.Windows.Forms.Label mImageLabel;
        private System.Windows.Forms.Label mDamageTextLabel;
        private System.Windows.Forms.Label mHealthTextLabel;
        private System.Windows.Forms.ComboBox mHealthCB;
        private System.Windows.Forms.ComboBox mDamageCB;
        private System.Windows.Forms.Label mHealthLabel;
        private System.Windows.Forms.Label mBackSlashLabel;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel mMainSS;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportCardXMLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportCardIMGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadCardXMLToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox mMobTypeTB;
        private System.Windows.Forms.Button mResetBtn;
        private System.Windows.Forms.Label mCardEffectLabel;
        private System.Windows.Forms.Label label3;
    }
}

